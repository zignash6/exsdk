//
//  FrameworkManager.swift
//  ExternalLoader
//
//  Created by Undrakonda Gopala Krishna on 05/02/21.
//

import Foundation
import PlayMetrics

protocol FrameworksProxyProtocol: AnyObject {
    func setup()
}

open class FrameworksManager: NSObject {
    
    @objc(sharedInstance) public static let shared = FrameworksManager()
    
    @objc private(set) lazy var playMetricsProxy = PlayMetricsProxy()
    
    open var isFirebaseEnable: Bool = false
    open var isPlayMatricsEnable: Bool = false
    
    @objc open func setupFrameworks() {
        createProxies()
        self.setupProxy(proxy: self.playMetricsProxy, enabled: isPlayMatricsEnable)
    }
    
    private func createProxies() {
        playMetricsProxy = PlayMetricsProxy()
    }

    private func setupProxy(proxy: FrameworksProxyProtocol?, enabled: Bool?) {
        proxy?.setup()
    }
}

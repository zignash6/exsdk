//
//  PlayMetricsProxy.swift
//  ExternalLoader
//
//  Created by Undrakonda Gopala Krishna on 05/02/21.
//

import Foundation
import PlayMetrics

final class PlayMetricsProxy: NSObject, FrameworksProxyProtocol {
    
    // MARK: - Attributes
    
    // MARK: - Methods

    func setup() {
        configurePlayMetricsSDK {
            self.registerInitEvent()
            self.registerOpenEvent()
        }
    }
    
    @objc func askForPermission() {
        PMTracker.ask(for: PMPermission.notification)
    }
    
    @objc func setAccountName(_ accountName: String?) {
            PMTracker.setAccountName(accountName)
    }
    
    @objc func registerLoginEvent() {
        guard let sportsAppEvent = PMSportsAppEvent.init(eventType: PMSportsEvent.LOG_IN) else { return }
        
        sportsAppEvent.sportsLogin = "TRUE"
        PMTracker.register(sportsAppEvent)
    }
    
    // MARK: - Private methods

       private func configurePlayMetricsSDK(_ completion: @escaping () -> Void) {
           PMTracker.initialize(withEndPoint: Constants.playMetricsSDKEnpoint)
           PMTracker.setDeviceID(UIDevice.current.identifierForVendor?.uuidString ?? "")
           PMTracker.setFrontEnd(Constants.frontEndIdentifier)
       }
    
    private func disablePlayMetricsSDK() {
        guard let unregisterEvent = PMSportsAppEvent(eventType: PMSportsEvent.UNREGISTER) else { return }

        unregisterEvent.deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
        unregisterEvent.frontEnd = Constants.frontEndIdentifier
        
        PMTracker.unRegisterFcmIdAppEvent(unregisterEvent, atUrl: Constants.playMetricsSDKEnpoint)
    }
    
    private func registerInitEvent() {
        guard let sportsAppEvent = PMSportsAppEvent(eventType: PMSportsEvent.INIT) else { return }

        sportsAppEvent.appInfo = PMAppInfo()
        sportsAppEvent.appInfo.appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
        sportsAppEvent.appInfo.appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        sportsAppEvent.deviceInfo = PMDeviceInfo()
        sportsAppEvent.deviceInfo.osName = UIDevice.current.systemName
        sportsAppEvent.deviceInfo.osVersion = UIDevice.current.systemVersion
        sportsAppEvent.deviceInfo.deviceType = UIDevice.current.model
        sportsAppEvent.deviceInfo.deviceVersion = UIDevice.current.modelName
        sportsAppEvent.deviceInfo.manufacturer = Constants.manufacturer
        sportsAppEvent.platform = Constants.platform
        sportsAppEvent.country = Locale.current.regionCode?.uppercased()
        sportsAppEvent.language = Locale.current.languageCode
        sportsAppEvent.timeZone = String(TimeZone.current.secondsFromGMT(for: Date()) * 1000)

        let documentsFolderURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        if let documentsFolderURLPath = documentsFolderURL?.path {
            let installDate = (try? FileManager.default.attributesOfItem(atPath: documentsFolderURLPath)[.creationDate] as? Date) ?? Date()
            sportsAppEvent.installDate = String(Int(installDate.timeIntervalSince1970))
        }

        PMTracker.register(sportsAppEvent)
    }
    
    private func registerOpenEvent() {
        guard let event = PMSportsAppEvent(eventType: PMSportsEvent.OPEN) else { return }

        PMTracker.register(event)
    }
}

// MARK: - Constants
private extension PlayMetricsProxy {
    struct Constants {
        static let frontEndIdentifier = "APP_NAME"
        static let playMetricsSDKEnpoint =
            "https://lcg-metricscollector.partygaming.com/api/rest/mcsgateway/v1/eventListener"
        static let platform = "ios"
        static let manufacturer = "Apple"
    }
}

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)

        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }

            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        return identifier
    }
}

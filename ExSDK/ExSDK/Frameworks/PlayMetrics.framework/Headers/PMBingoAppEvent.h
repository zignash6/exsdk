//
//  PMBingoAppEvent.h
//  PlayMetrics
//
//  Created by Manish Kumar on 22/06/2018.
//  Copyright © 2018 GVC. All rights reserved.
//

#import <PlayMetrics/PlayMetrics.h>

typedef NS_ENUM(NSUInteger, PMBingoEvent) {
    PMBingoEvent_NONE,
    PMBingoEvent_BROWSE,
    PMBingoEvent_DEPOSIT,
    PMBingoEvent_INIT,
    PMBingoEvent_LOG_IN,
    PMBingoEvent_OPEN,
    PMBingoEvent_PLAYED_TABLE,
    PMBingoEvent_OPENED_GAME,
    PMBingoEvent_REGISTRATION,
    PMBingoEvent_TOURNAMENT,
    PMBingoEvent_CLICKED,
    PMBingoEvent_UPDATE_COUNTRY,
    PMBingoEvent_UPDATE_FCM,
    PMBingoEvent_LOGIN_FAILED,
    PMBingoEvent_SESSION_CLOSE_BALANCE,
    PMBingoEvent_UPDATE_STATE,
    PMBingoEvent_UNREGISTER
};

// TODO: Need to update in the next release
typedef NS_ENUM(NSUInteger, PMBingoPlayProductType) {
    PMBingoPlayProductType_NONE,
    PMBingoPlayProductType_SLOTS,
    PMBingoPlayProductType_BLACKJACK,
    PMBingoPlayProductType_ROULETTE,
    PMBingoPlayProductType_JACKPOTS,
    PMBingoPlayProductType_PROGRESSIVE,
    PMBingoPlayProductType_VIDEO_POKER,
    PMBingoPlayProductType_BACCARET,
    PMBingoPlayProductType_BLACKJACK_LIVE,
    PMBingoPlayProductType_ROULETTE_LIVE,
    PMBingoPlayProductType_JACKPOTS_LIVE,
    PMBingoPlayProductType_MARVEL_LIVE,
    PMBingoPlayProductType_VIDEO_POKER_LIVE,
    PMBingoPlayProductType_OTHER
};

typedef NS_ENUM(NSUInteger, PMBingoPlayNetWorkType) {
    PMBingoPlayNetWorkType_NONE,
    PMBingoPlayNetWorkType_NET_ENT
};

typedef NS_ENUM(NSUInteger, PMBingoDepositState) {
    PMBingoDepositState_NONE,
    PMBingoDepositState_C
};

/**
 Contains details of the events specific to Bingo app to register with PlayMetrics.
 */
@interface PMBingoAppEvent : PMAppEvent
@property(nonatomic, copy)NSString *bingoBrowseCategory;
@property(nonatomic, copy)NSString *bingoBrowseProductType;
@property(nonatomic, copy)NSString *bingoBrowseProductTitle;

@property(nonatomic, copy)NSString *bingoDepositAmount;
@property(nonatomic, copy)NSString *bingoDepositCurrency;

@property(nonatomic, copy)NSString *bingoPlayedProductTitle;
@property(nonatomic, copy)NSString *bingoPlayedNetworkGame;

@property(nonatomic, copy)NSString *bingoRegUserId;
@property(nonatomic, copy)NSString *bingoRegUserCategory;
@property(nonatomic, copy)NSString *bingoRegUserChannel;
@property(nonatomic, copy)NSString *bingoLogin;

@property(nonatomic, assign)PMBingoPlayProductType playProductType;
@property(nonatomic, assign)PMBingoPlayNetWorkType playNetworkType;
@property(nonatomic, assign)PMBingoDepositState depositState;

/**
 :nodoc:
 */
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithEventType:(PMBingoEvent)eventType NS_DESIGNATED_INITIALIZER;
@end

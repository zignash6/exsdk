//
//  PMDeviceInfo.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 23/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMBaseJSONModel.h"

/**
 Contains basic device information to indentify a device for sending push notification.
 */
@interface PMDeviceInfo : PMBaseJSONModel

/**
 OS name of the device.
 */
@property(nonatomic, copy)NSString *osName;
/**
 OS version of the device.
 */
@property(nonatomic, copy)NSString *osVersion;
/**
 Manufacturer of the device.
 */
@property(nonatomic, copy)NSString *manufacturer;
/**
 Type of device.
 */
@property(nonatomic, copy)NSString *deviceType;
/**
 Model version of device.
 */
@property(nonatomic, copy)NSString *deviceVersion;

+(instancetype)defaultDeviceInfo;
@end

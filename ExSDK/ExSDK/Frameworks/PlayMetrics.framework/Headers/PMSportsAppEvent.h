//
//  PMSportsAppEvent.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 26/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <PlayMetrics/PlayMetrics.h>

typedef NS_ENUM(NSUInteger, PMSportsEvent) {
    PMSportsEvent_NONE,
    PMSportsEvent_BROWSE,
    PMSportsEvent_BET_SLIP,
    PMSportsEvent_DEPOSIT,
    PMSportsEvent_INIT,
    PMSportsEvent_LOG_IN,
    PMSportsEvent_OPEN,
    PMSportsEvent_PLACED_BET,
    PMSportsEvent_BET_CONFIRM,
    PMSportsEvent_REGISTRATION,
    PMSportsEvent_SESSION_CLOSE_BALANCE,
    PMSportsEvent_CLICKED,
    PMSportsEvent_UPDATE_COUNTRY,
    PMSportsEvent_UPDATE_FCM,
    PMSportsEvent_UPDATE_STATE,
    PMSportsEvent_UNREGISTER
};


typedef NS_ENUM(NSUInteger, PMSportsPlayProductType) {
    PMSportsPlayProductType_NONE,
    PMSportsPlayProductType_FOOTBALL,
    PMSportsPlayProductType_TENNIS,
    PMSportsPlayProductType_BASKETBALL,
    PMSportsPlayProductType_VOLLEYBALL,
    PMSportsPlayProductType_PREMIER_LEAGUE,
    PMSportsPlayProductType_LEAGUE,
    PMSportsPlayProductType_BLACKJACK,
    PMSportsPlayProductType_ROULETTE,
    PMSportsPlayProductType_OTHER
};

typedef NS_ENUM(NSUInteger, PMSportsDepositState) {
    PMSportsDepositState_NONE,
    PMSportsDepositState_C
};

typedef NS_ENUM(NSUInteger, PMSportsDepositCurrency) {
    PMSportsDepositCurrency_NONE,
    PMSportsDepositCurrency_USD,
    PMSportsDepositCurrency_EUR
};

/**
 Contains details of the events specific to Sports app to register with PlayMetrics.
 */
@interface PMSportsAppEvent : PMAppEvent

@property(nonatomic, copy)NSString *sportsBrowseCategory;
@property(nonatomic, copy)NSString *sportsBrowseSubCategory;

@property(nonatomic, copy)NSString *sportsBetSlipEventName;
@property(nonatomic, copy)NSString *sportsBetSlipMarketName;
@property(nonatomic, copy)NSString *sportsBetSlipResultName;
@property(nonatomic, copy)NSString *sportsBetSlipSportName;
@property(nonatomic, copy)NSString *sportsBetSlipEventId;
@property(nonatomic, copy)NSString *sportsBetSlipMarketId;
@property(nonatomic, copy)NSString *sportsBetSlipResultId;
@property(nonatomic, copy)NSString *sportsBetSlipSportId;

@property(nonatomic, copy)NSString *sportsBetConfirmationAmount;
@property(nonatomic, copy)NSString *sportsBetConfirmationBetType;
@property(nonatomic, copy)NSString *sportsBetConfirmationBetCurrency;
@property(nonatomic, copy)NSString *sportsBetConfirmationBets;

@property(nonatomic, copy)NSString *sportsBetPlacementSportName;
@property(nonatomic, copy)NSString *sportsBetPlacementPremierLeague;
@property(nonatomic, copy)NSString *sportsBetPlacementLeague;
@property(nonatomic, copy)NSString *sportsBetPlacementEventIDBacked;

@property(nonatomic, copy)NSString *sportsDepositAmount;
@property(nonatomic, copy)NSString *sportsDepositMethod;
@property(nonatomic, copy)NSString *sportsFirstTimeDeposit;


@property(nonatomic, copy)NSString *sportsRegUserId;
@property(nonatomic, copy)NSString *sportsRegUserCategory;
@property(nonatomic, copy)NSString *sportsRegUserChannel;
@property(nonatomic, copy)NSString *sportsLogin;

@property(nonatomic, assign)PMSportsDepositCurrency depositCurrency;
@property(nonatomic, assign)PMSportsDepositState depositState;
@property(nonatomic, assign)PMSportsPlayProductType playProductType;

/**
 :nodoc:
 */
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithEventType:(PMSportsEvent)eventType NS_DESIGNATED_INITIALIZER;
@end

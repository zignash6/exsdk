//
//  PMCasinoAppEvent.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 26/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <PlayMetrics/PlayMetrics.h>

typedef NS_ENUM(NSUInteger, PMCasinoEvent) {
    PMCasinoEvent_NONE,
    PMCasinoEvent_BROWSE,
    PMCasinoEvent_DEPOSIT,
    PMCasinoEvent_INIT,
    PMCasinoEvent_LOG_IN,
    PMCasinoEvent_OPEN,
    PMCasinoEvent_PLAYED_TABLE,
    PMCasinoEvent_OPENED_GAME,
    PMCasinoEvent_REGISTRATION,
    PMCasinoEvent_TOURNAMENT,
    PMCasinoEvent_CLICKED,
    PMCasinoEvent_UPDATE_COUNTRY,
    PMCasinoEvent_UPDATE_FCM,
    PMCasinoEvent_LOGIN_FAILED,
    PMCasinoEvent_SESSION_CLOSE_BALANCE,
    PMCasinoEvent_UPDATE_STATE,
    PMCasinoEvent_UNREGISTER
};

typedef NS_ENUM(NSUInteger, PMCasinoPlayProductType) {
    PMCasinoPlayProductType_NONE,
    PMCasinoPlayProductType_SLOTS,
    PMCasinoPlayProductType_BLACKJACK,
    PMCasinoPlayProductType_ROULETTE,
    PMCasinoPlayProductType_JACKPOTS,
    PMCasinoPlayProductType_PROGRESSIVE,
    PMCasinoPlayProductType_VIDEO_POKER,
    PMCasinoPlayProductType_BACCARET,
    PMCasinoPlayProductType_BLACKJACK_LIVE,
    PMCasinoPlayProductType_ROULETTE_LIVE,
    PMCasinoPlayProductType_JACKPOTS_LIVE,
    PMCasinoPlayProductType_MARVEL_LIVE,
    PMCasinoPlayProductType_VIDEO_POKER_LIVE,
    PMCasinoPlayProductType_OTHER
};

typedef NS_ENUM(NSUInteger, PMCasinoPlayNetWorkType) {
    PMCasinoPlayNetWorkType_NONE,
    PMCasinoPlayNetWorkType_NET_ENT
};

typedef NS_ENUM(NSUInteger, PMCasinoDepositState) {
    PMCasinoDepositState_NONE,
    PMCasinoDepositState_C
};

/**
 Contains details of the events specific to Casino app to register with PlayMetrics.
 */
@interface PMCasinoAppEvent : PMAppEvent

@property(nonatomic, copy)NSString *casinoBrowseCategory;
@property(nonatomic, copy)NSString *casinoBrowseProductType;
@property(nonatomic, copy)NSString *casinoBrowseProductTitle;

@property(nonatomic, copy)NSString *casinoDepositAmount;
@property(nonatomic, copy)NSString *casinoDepositCurrency;

@property(nonatomic, copy)NSString *casinoPlayedProductTitle;
@property(nonatomic, copy)NSString *casinoPlayedNetworkGame;

@property(nonatomic, copy)NSString *casinoRegUserId;
@property(nonatomic, copy)NSString *casinoRegUserCategory;
@property(nonatomic, copy)NSString *casinoRegUserChannel;
@property(nonatomic, copy)NSString *casinoLogin;

@property(nonatomic, assign)PMCasinoPlayProductType playProductType;
@property(nonatomic, assign)PMCasinoPlayNetWorkType playNetworkType;
@property(nonatomic, assign)PMCasinoDepositState depositState;

/**
 :nodoc:
 */
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithEventType:(PMCasinoEvent)eventType NS_DESIGNATED_INITIALIZER;
@end

//
//  PMBaseJSONModel.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 23/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 Base model class. Child classes of this class can be represnted in JSON form.
 */
@interface PMBaseJSONModel : NSObject

/**
 List of property names which will participate in JSON representation. Subclasses should override this method to return respective  property names.
 @return `NSArray` containing property names as `NSString`.
 @note Default implementation returns an empty `NSArray`.
 */
- (NSArray <NSString *> *)propertyNamesForJSONRepresentation;

@end

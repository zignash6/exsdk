//
//  PMLogLevel.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 21/12/2017.
//  Copyright © 2017 GVC. All rights reserved.
//

#ifndef PMLogLevel_h
#define PMLogLevel_h

typedef NS_ENUM(NSUInteger, PMLogLevel) {
    PMLogLevel_NONE,
    PMLogLevel_ERROR,
    PMLogLevel_WARNING,
    PMLogLevel_INFO,
    PMLogLevel_ALL
};



#endif /* PMLogLevel_h */

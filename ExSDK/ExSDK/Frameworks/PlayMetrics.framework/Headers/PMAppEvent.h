//
//  PMAppEvent.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 22/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PMBaseJSONModel.h"
@class PMAppInfo, PMDeviceInfo;

/**
 Abstract class for registering events. Subclasses need to provide `eventType` and `product`.
 @note Do not initialize this class dirctly. Use pre-defined Concrete subclasses namely `PMPokerAppEvent`, `PMSportsAppEvent`, `PMCasinoAppEvent`.
 */
@interface PMAppEvent : PMBaseJSONModel


@property(nonatomic, copy)NSString *accountName;
@property(nonatomic, copy)NSString *deviceId;
@property(nonatomic, copy)NSString *fcmId;
@property(nonatomic, copy)NSString *frontEnd;
@property(nonatomic, copy)NSString *state;
@property(nonatomic, copy)NSString *campaignId;
@property(nonatomic, copy)NSString *lastSessionCloseBalance;

@property(nonatomic, readonly)NSString *eventType;
@property(nonatomic, readonly)NSString *product;


// USER ONE-TIME attributes ---- START

@property(nonatomic, copy)NSString *userCategory;
@property(nonatomic, copy)NSString *country;
@property(nonatomic, copy)NSString *installDate;
@property(nonatomic, copy)NSString *language;
@property(nonatomic, copy)NSString *timeZone;
@property(nonatomic, copy)NSString *platform;

@property(nonatomic, strong)PMDeviceInfo *deviceInfo;
@property(nonatomic, strong)PMAppInfo *appInfo;

// USER ONE-TIME attributes ---- END


@end

//
//  PlayMetrics.h
//  PlayMetrics
//
//  Created by Vishal Singh Panwar on 22/10/17.
//  Copyright © 2017 GVC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PlayMetrics.
FOUNDATION_EXPORT double PlayMetricsVersionNumber;

//! Project version string for PlayMetrics.
FOUNDATION_EXPORT const unsigned char PlayMetricsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PlayMetrics/PublicHeader.h>
#import <PlayMetrics/PMTracker.h>
#import <PlayMetrics/PMAppEvent.h>
#import <PlayMetrics/PMPokerAppEvent.h>
#import <PlayMetrics/PMSportsAppEvent.h>
#import <PlayMetrics/PMCasinoAppEvent.h>
#import <PlayMetrics/PMBingoAppEvent.h>
#import <PlayMetrics/PMAppInfo.h>
#import <PlayMetrics/PMDeviceInfo.h>
#import <PlayMetrics/PMBaseJSONModel.h>


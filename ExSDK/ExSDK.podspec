Pod::Spec.new do |spec|
  spec.name          = 'ExSDK'
  spec.version       = '1.0.0'
  spec.license      = { :type => 'MIT', :text => <<-LICENSE
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  LICENSE
  }

  spec.homepage      = 'https://zignash@bitbucket.org/zignash6/exsdk'
  spec.author       = { "undrakonda_krishna" => "undrakonda.krishna@ivycomptech.com" }
  spec.summary       = 'Sample Testing'
  spec.source        = { :git => 'https://zignash@bitbucket.org/zignash6/exsdk.git', :tag => '1.0.0' }
  spec.swift_version = '5.0'
  spec.ios.deployment_target  = '9.0'
  spec.source_files  = "ExSDK/**/*.{h,m,swift}"

  # spec.framework  = "PlayMetrics"
  # spec.static_framework = true
  spec.vendored_frameworks = 'PlayMetrics.framework'

  # spec.dependency "PlayMetrics"

end